# comics to rss

Verwende die öffentlichen Informationen von https://mangaread.org/ und erstelle einen RSS-feed um einen bestimmten Comic zu folgen.

RSS-feeds:

- [NatoManga - Tales Of Demons And Gods](https://neroburner.gitlab.io/comics_to_rss/tales-of-demons-and-gods.xml) - [WebSite](https://www.natomanga.com/manga/tales-of-demons-and-gods/)
- [NatoManga - Solo Leveling](https://neroburner.gitlab.io/comics_to_rss/solo-leveling.xml) - [WebSite](https://www.natomanga.com/manga/solo-leveling)
- [NatoManga - Solo Leveling Ragnarok](https://neroburner.gitlab.io/comics_to_rss/solo-leveling.xml) - [WebSite](https://www.natomanga.com/manga/solo-leveling)
- [NatoManga - Tensei Shitara Slime Datta Ken](https://neroburner.gitlab.io/comics_to_rss/tensai-shitara-silme-datta-ken.xml) - [WebSite](https://www.natomanga.com/manga/tensei-shitara-slime-datta-ken)
- [NatoManga - Berserk of Gluttony](https://neroburner.gitlab.io/comics_to_rss/berserk_of_gluttony.xml) - [WebSite](https://www.natomanga.com/manga/berserk-of-gluttony)
- [NatoManga - Dungeon Reset](https://neroburner.gitlab.io/comics_to_rss/dungeon_reset.xml) - [WebSite](https://www.natomanga.com/manga/dungeon-reset)
- [NatoManga - Onepunch-Man (One)](https://neroburner.gitlab.io/comics_to_rss/onepunchman_one.xml) - [WebSite](https://www.natomanga.com/manga/onepunch-man-one)
- [NatoManga - Onepunch-Man](https://neroburner.gitlab.io/comics_to_rss/onepunchman.xml) - [WebSite](https://www.natomanga.com/manga/onepunch-man)
- [NatoManga - The Strongest Florist](https://neroburner.gitlab.io/comics_to_rss/the-strongest-florist.xml) - [WebSite](https://www.natomanga.com/manga/the-strongest-florist)
- [NatoManga - The Player That Can't Level Up](https://neroburner.gitlab.io/comics_to_rss/player_cant_level_up.xml) - [WebSite](chttps://www.natomanga.com/manga/the-player-that-can-t-level-up)
- [NatoManga - Tsuki Ga Michibiku Isekai Douchuu](https://neroburner.gitlab.io/comics_to_rss/tsuki_ga_michibiku.xml) - [WebSite](https://www.natomanga.com/manga/tsuki-ga-michibiku-isekai-douchuu)
- [NatoManga - Reincarnation - It will be All Out if I Go to Another World](https://neroburner.gitlab.io/comics_to_rss/reincarnation_all_out.xml) - [WebSite](https://readmanganato.com/manga-qm951521)
- [NatoManga - The Max Level Hero Has Returned!](https://neroburner.gitlab.io/comics_to_rss/max_level_hero_returned.xml) - [WebSite](https://www.natomanga.com/manga/the-max-level-hero-has-returned)
- [NatoManga - I was reincarnated as the 7th Prince so I will perfect my magic as I please](https://neroburner.gitlab.io/comics_to_rss/reincarnated_perfect_magic.xml) - [WebSite](chttps://www.natomanga.com/manga/tensei-shitara-dai-nana-ouji-dattanode-kimamani-majutsu-o-kiwamemasu)
  - original title: Tensei Shitara Dai Nana Ouji Dattanode, Kimamani Majutsu O Kiwamemasu
- [NatoManga - Capture a Different World with the Power of Craft Games!](https://neroburner.gitlab.io/comics_to_rss/reincarnated_survival.xml) - [WebSite](https://www.natomanga.com/manga/goshujin-sama-to-yuku-isekai-survival)
  - original title: Goshujin-Sama To Yuku Isekai Survival!
- [NatoManga - Catastrophic Necromancer](https://neroburner.gitlab.io/comics_to_rss/catastrophic_necromancer.xml) - [WebSite](https://www.natomanga.com/manga/catastrophic-necromancer)
- [NatoManga - Release That Witch](https://neroburner.gitlab.io/comics_to_rss/release_that_witch.xml) - [WebSite](https://www.natomanga.com/manga/release-that-witch)
- [NatoManga - The Eminence In Shadow](https://neroburner.gitlab.io/comics_to_rss/the_eminence_in_shadow.xml) - [WebSite](https://www.natomanga.com/manga/the-eminence-in-shadow)
- [NatoManga - Black Summoner](https://neroburner.gitlab.io/comics_to_rss/black_summoner.xml) - [WebSite](https://www.natomanga.com/manga/black-summoner)
  - original title: Kuro No Shoukanshi
- [NatoManga - Solo Farming in the Tower](https://neroburner.gitlab.io/comics_to_rss/solo_farming_in_the_tower.xml) - [WebSite](chttps://www.natomanga.com/manga/solo-farming-in-the-tower)
- [NatoManga - The Exiled Reincarnated Heavy Knight Is Unrivaled In Game Knowledge](https://neroburner.gitlab.io/comics_to_rss/exiled_heavy_knight.xml) - [WebSite](https://www.natomanga.com/manga/the-exiled-reincarnated-heavy-knight-is-unrivaled-in-game-knowledge)
