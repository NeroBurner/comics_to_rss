# install google chrome
#wget -q -O - https://dl-ssl.google.com/linux/linux_signing_key.pub | apt-key add -
#sh -c 'echo "deb [arch=amd64] http://dl.google.com/linux/chrome/deb/ stable main" >> /etc/apt/sources.list.d/google-chrome.list'
#apt-get -y update
#apt-get install -y google-chrome-stable
# install chromedriver
#apt-get install -yqq unzip
#wget -O /tmp/chromedriver.zip http://chromedriver.storage.googleapis.com/`curl -sS chromedriver.storage.googleapis.com/LATEST_RELEASE`/chromedriver_linux64.zip
#unzip /tmp/chromedriver.zip chromedriver -d /usr/local/bin/

python -V               # Print out python version for debugging
pip install requests mako # selenium
apt-get update
apt-get install -y locales
sed -i 's/^# *\(en_US.UTF-8\)/\1/' /etc/locale.gen
sed -i 's/^# *\(de_AT.UTF-8\)/\1/' /etc/locale.gen
locale-gen
