import argparse
import requests
import html
import re
import time
import os
import sys
import locale
from datetime import datetime
from datetime import timedelta
# generate html
import mako.exceptions
from mako.template import Template

def parse_entry_mangaread(list_entry, referrer="", verbose=False):
    if verbose:
        print("parse_entry_mangaread: begin")

    #    <a href="https://www.mangaread.org/manga/tales-of-demons-and-gods/chapter-302-5/">Chapter 302.5</a>
    #    <span class="chapter-release-date"><i>3 hours ago</i></span>

    #<li class="wp-manga-chapter  ">
    #    <a href="https://www.mangaread.org/manga/tales-of-demons-and-gods/chapter-309-5/">
    #        Chapter 309.5								</a>
    #                                        <span class="chapter-release-date">
    #            <i>3 days ago</i>									</span>
    #</li>
    title = re.search('(?:">[\t\n]*)(.*?)(?:[\t]*</a>)', list_entry)[1]
    description = title
    link = re.search('(?:<a href=")([^"]*)', list_entry)[1]

    author = "MangaReader"
    pubDateIn = re.search('(?:<span class="chapter-release-date">[\t\n]*<i>)(.*?)(?:</i>[\t\n]*</span>)', list_entry)[1]

    # in
    # - 1 min ago
    # - 45 mins ago
    # - 3 hours ago
    # - 1 day ago
    # - 2 days ago
    # - 18.11.2020
    # out Sun, 18 Nov 2020 00:00:00 +0000
    locale.setlocale(locale.LC_TIME, 'en_US.UTF-8')
    pub_mins  = re.search('([0-9]+) min[s]? ago', pubDateIn)
    pub_hours = re.search('([0-9]+) hour[s]? ago', pubDateIn)
    pub_days  = re.search('([0-9]+) day[s]? ago', pubDateIn)
    if pub_mins:
        mins_ago = int(pub_mins[1])
        mins_delta = timedelta(minutes=mins_ago)
        pubDate = (datetime.now()-mins_delta).strftime('%a, %d %b %Y 00:00:00 +0000')
    elif pub_hours:
        hours_ago = int(pub_hours[1])
        hours_delta = timedelta(hours=hours_ago)
        pubDate = (datetime.now()-hours_delta).strftime('%a, %d %b %Y 00:00:00 +0000')
    elif pub_days:
        days_ago = int(pub_days[1])
        days_delta = timedelta(days=days_ago)
        pubDate = (datetime.today()-days_delta).strftime('%a, %d %b %Y 00:00:00 +0000')
    else:
        last_time_updated = datetime.strptime(pubDateIn, '%d.%m.%Y')
        pubDate = last_time_updated.strftime('%a, %d %b %Y 00:00:00 +0000')

    entry = {
            "title": title,
            "description": description,
            "link": referrer + link,
            "author": author,
            "pubDate": pubDate,
            }
    if verbose:
        print("parse_entry_mangaread: found entry with title: '{}'".format(title))
    return entry

def parse_entry_imperfectcomic(list_entry, referrer="", verbose=False):
    if verbose:
        print("parse_entry_imperfectcomic: begin")

    # <a href="https://imperfectcomic.com/comic/the-strongest-florist/chapter-86/">
    # Chapter 86 </a>
    # <span class="chapter-release-date">
    # <a href="https://imperfectcomic.com/comic/the-strongest-florist/chapter-86/" title="1 day ago" class="c-new-tag"><!-- --></a> </span>
    # <span class="view"><i class="fa fa-eye"></i> 3191</span>

    # <a href="https://imperfectcomic.com/comic/the-strongest-florist/chapter-85/">
    # Chapter 85 </a>
    # <span class="chapter-release-date">
    # <i>October 4, 2021</i> </span>
    # <span class="view"><i class="fa fa-eye"></i> 5391</span>

    title = re.search('(?:">[\t\n]*)(.*?)(?:[\t ]*</a>)', list_entry)[1]
    description = title
    link = re.search('(?:<a href=")([^"]*)', list_entry)[1]

    author = "ImperfectComics"
    matches = re.search('(?:<span class="chapter-release-date">[\t\n]*<a href=".*" title=")(.*?)(?:["[ \t]*class="c-new-tag")', list_entry)
    if matches:
        pubDateIn = matches[1]
    else:
        pubDateIn = re.search('(?:<span class="chapter-release-date">[\t\n]*<i>)(.*?)(?:[ \t\n]*</i>[ \t\n]*</span>)', list_entry)[1]

    # in
    # - 1 min ago
    # - 45 mins ago
    # - 3 hours ago
    # - 1 day ago
    # - 2 days ago
    # - September 25, 2021
    # out Sun, 18 Nov 2020 00:00:00 +0000
    locale.setlocale(locale.LC_TIME, 'en_US.UTF-8')
    pub_mins  = re.search('([0-9]+) min[s]? ago', pubDateIn)
    pub_hours = re.search('([0-9]+) hour[s]? ago', pubDateIn)
    pub_days  = re.search('([0-9]+) day[s]? ago', pubDateIn)
    if pub_mins:
        mins_ago = int(pub_mins[1])
        mins_delta = timedelta(minutes=mins_ago)
        pubDate = (datetime.now()-mins_delta).strftime('%a, %d %b %Y 00:00:00 +0000')
    elif pub_hours:
        hours_ago = int(pub_hours[1])
        hours_delta = timedelta(hours=hours_ago)
        pubDate = (datetime.now()-hours_delta).strftime('%a, %d %b %Y 00:00:00 +0000')
    elif pub_days:
        days_ago = int(pub_days[1])
        days_delta = timedelta(days=days_ago)
        pubDate = (datetime.today()-days_delta).strftime('%a, %d %b %Y 00:00:00 +0000')
    else:
        last_time_updated = datetime.strptime(pubDateIn, '%B %d, %Y')
        pubDate = last_time_updated.strftime('%a, %d %b %Y 00:00:00 +0000')

    entry = {
            "title": title,
            "description": description,
            "link": referrer + link,
            "author": author,
            "pubDate": pubDate,
            }
    if verbose:
        print("parse_entry_imperfectcomic: found entry with title: '{}'".format(title))
    return entry


def parse_site_imperfectcomic(url, referrer="", verbose=False):
    url="https://imperfectcomic.com/comic/the-strongest-florist"
    tag = url.replace("https://imperfectcomic.com/comic/", "")
    url_ajax = "https://imperfectcomic.com/comic/{}/ajax/chapters/".format(tag)

    r_main = requests.get("https://imperfectcomic.com/comic/the-strongest-florist")
    html_main = r_main.text

    # debug write file to html
    #with open("the-strongest-florist.html", "w") as f:
    #    f.write(html)

    # read the debug file back
    #with open("the-strongest-florist_main.html", "r") as f:
    #    html_main = f.read()

    r = requests.post("https://imperfectcomic.com/comic/the-strongest-florist/ajax/chapters/", headers={"Accept": "*/*"})
    html = r.text
    print("---")
    print(html)
    print("---")

    # debug write file to html
    #with open("the-strongest-florist.html", "w") as f:
    #    f.write(html)

    # read the debug file back
    #with open("the-strongest-florist.html", "r") as f:
    #    html = f.read()

    # get title in the following form
    # <title>The Strongest Florist - Imperfect Comic</title>

    manga_title = re.search('(?:<title>)(.*?)(?:[\t ]*</title>)', html_main)[1]
    manga_title = manga_title.replace(" - Imperfect Comic", "")

    # generate channel information
    locale.setlocale(locale.LC_TIME, 'en_US.UTF-8')
    channel_pub_date = datetime.now().strftime('%a, %d %b %Y %H:%M:%S +0000')
    channel = {
            "title": "IC " + manga_title,
            "link": url,
            "description": "ImperfectComic " + manga_title,
            "language": "en-us",
            "copyright": "ImperfectComic",
            "pubDate": channel_pub_date,
            "image": {
                    "url": "https://imperfectcomic.com/wp-content/uploads/2021/03/cropped-logo0-32x32.png",
                    "title": "ImperfectComic",
                    "link": url,
                    },
            }

    # <li class="wp-manga-chapter    ">
    # <a href="https://imperfectcomic.com/comic/the-strongest-florist/chapter-86/">
    # Chapter 86 </a>
    # <span class="chapter-release-date">
    # <a href="https://imperfectcomic.com/comic/the-strongest-florist/chapter-86/" title="1 day ago" class="c-new-tag"><!-- --></a> </span>
    # <span class="view"><i class="fa fa-eye"></i> 3191</span>
    # </li>
    # <li class="wp-manga-chapter    ">
    # <a href="https://imperfectcomic.com/comic/the-strongest-florist/chapter-85/">
    # Chapter 85 </a>
    # <span class="chapter-release-date">
    # <i>October 4, 2021</i> </span>
    # <span class="view"><i class="fa fa-eye"></i> 5391</span>
    # </li>
    re_list_elements = '(?:<li class="wp-manga-chapter    ">)(.*?)(?:</li>)'
    matches = re.findall(re_list_elements, html, re.DOTALL)

    if verbose:
        print("parse_site_imperfectcomic: number of matches found in site: {}".format(len(matches)))
    if len(matches) == 0:
        raise ValueError("found no entries for site: '{}'".format(url))

    data = [parse_entry_imperfectcomic(x, referrer, verbose) for x in matches]

    if verbose:
        print("parse_site_imperfectcomic: generated data")
    return channel, data

def parse_site_mangaread(url, referrer="", verbose=False):
    if verbose:
        print("parse_site_mangaread: parsing url: '{}'".format(url))

    try:
        #try:
        from selenium import webdriver
        firefox_options = webdriver.FirefoxOptions()
        firefox_options.headless = True
        firefox_options.add_argument("--disable-gpu")
        browser = webdriver.Firefox(options=firefox_options)
        #except:
        #    chrome_options = webdriver.ChromeOptions()
        #    chrome_options.add_argument('--no-sandbox')
        #    chrome_options.add_argument('--window-size=1420,1080')
        #    chrome_options.add_argument('--headless')
        #    chrome_options.add_argument('--disable-gpu')
        #    chrome_options.add_argument('--allow-profiles-outside-user-dir')
        #    chrome_options.add_argument('--user-data-dir=./chrome_data')
        #    browser = webdriver.Chrome(options=chrome_options)
        browser.get(url)
        html = browser.page_source
    except:
        browser.close()
        raise
    finally:
        browser.close()

    # debug write file to html
    #with open("mangaread.html", "w") as f:
    #    f.write(html)

    # read the debug file back
    #with open("mangaread.html", "r") as f:
    #    html = f.read()

    locale.setlocale(locale.LC_TIME, 'en_US.UTF-8')
    channel_pub_date = datetime.now().strftime('%a, %d %b %Y %H:%M:%S +0000')
    tag = url.split("/")[-1]
    channel = {
            "title": "MR " + tag,
            "link": url,
            "description": "Mangaread " + tag,
            "language": "en-us",
            "copyright": "Mangaread",
            "pubDate": channel_pub_date,
            "image": {
                    "url": "https://www.mangaread.org/wp-content/uploads/2017/10/llogos.png",
                    "title": "Mangaread",
                    "link": url,
                    },
            }
    # <li class="wp-manga-chapter  ">
    #    <a href="https://www.mangaread.org/manga/tales-of-demons-and-gods/chapter-302-5/">Chapter 302.5</a>
    #    <span class="chapter-release-date"><i>3 hours ago</i></span>
    #</li>
    re_list_elements = '(?:<li class="wp-manga-chapter  ">)(.*?)(?:</li>)'
    matches = re.findall(re_list_elements, html, re.DOTALL)

    if verbose:
        print("parse_site_mangaread: number of matches found in site: {}".format(len(matches)))
    if len(matches) == 0:
        raise ValueError("found no entries for site: '{}'".format(url))

    data = [parse_entry_mangaread(x, referrer, verbose) for x in matches]

    if verbose:
        print("parse_site_mangaread: generated data")
    return channel, data


def parse_entry_manganelo(list_entry, referrer="", verbose=False):
    if verbose:
        print("parse_entry_manganelo: begin")
    if verbose:
        print("parse_entry_manganelo: list_entry:", list_entry)
    entry = dict()

    #   <a rel="nofollow" class="chapter-name text-nowrap" href="https://manganelo.com/chapter/murabito_desu_ga_nani_ka/chapter_38" title="Murabito desu ga Nani ka? chapter Chapter 38">Chapter 38</a>
    #   <span class="chapter-view text-nowrap">24,593</span>
    #   <span class="chapter-time text-nowrap" title="Dec 28,2020 04:12">Dec 28,20</span>
    # in newer versions the 'fn-cover-item-time' part was added
    #   <span class="chapter-time text-nowrap fn-cover-item-time" data-fn-time="1734994797" title="Dec 23,2024 22:12">Dec 23,2024</span>\r\n        '

    title = re.search('(?:">[\t\n]*)(.*?)(?:[\t]*</a>)', list_entry)[1]
    title = html.escape(title)
    description = title
    link = re.search('(?:<a .* href=")([^"]*)', list_entry)[1]

    author = "Manganelo"
    pubDateIn = re.search('(?:<span class="chapter-time text-nowrap.*" title=")([^"]*)(?:">)', list_entry)[1]

    # in
    # Dec 28,2020 04:12
    # out Mon, 28 Dec 2020 04:12:00 +0000
    locale.setlocale(locale.LC_TIME, 'en_US.UTF-8')
    last_time_updated = datetime.strptime(pubDateIn, '%b %d,%Y %H:%M')
    pubDate = last_time_updated.strftime('%a, %d %b %Y %H:%M:00 +0000')

    entry = {
            "title": title,
            "description": description,
            "link": referrer + link,
            "author": author,
            "pubDate": pubDate,
            }
    if verbose:
        print("parse_entry_manganelo: found entry with title: '{}'".format(title))
    return entry


def parse_entry_mangakakalot(list_entry, referrer="", verbose=False):
    if verbose:
        print("parse_entry_mangakakalot: begin")
    if verbose:
        print("parse_entry_mangakakalot: list_entry:", list_entry)
    entry = dict()

    # <div class="row">
    #     <span><a href="https://www.mangakakalot.gg/manga/tales-of-demons-and-gods/chapter-0"
    #             title="Tales Of Demons And Gods Chapter 0">Chapter 0</a></span>
    #     <span> 166 </span>
    #     <span title="Feb-22-2025 04:19">2 day ago</span>
    # </div>

    title = re.search('(?:">[\t\n]*)(.*?)(?:[\t]*</a>)', list_entry)[1]
    title = html.escape(title)
    description = title
    link = re.search('(?:<a .*href=")([^"]*)', list_entry)[1]

    author = "MangaKakalot"
    pubDateIn = re.search('(?:<span title=")([^"]*)(?:">)', list_entry)[1]

    # in
    # Feb-22-2025 04:19
    # out Sat, 22 Feb 2024 04:19:00 +0000
    locale.setlocale(locale.LC_TIME, 'en_US.UTF-8')
    last_time_updated = datetime.strptime(pubDateIn, '%b-%d-%Y %H:%M')
    pubDate = last_time_updated.strftime('%a, %d %b %Y %H:%M:00 +0000')

    entry = {
            "title": title,
            "description": description,
            "link": referrer + link,
            "author": author,
            "pubDate": pubDate,
            }
    if verbose:
        print("parse_entry_mangakakalot: found entry with title: '{}'".format(title))
    return entry


def parse_site_manganelo(url, referrer="", verbose=False):
    if verbose:
        print("parse_site_manganelo: parsing url: '{}'".format(url))
    r = requests.get(url, timeout=(3.05, 27))
    html = r.text

    # debug write file to html
    #with open("manganelo.html", "w") as f:
    #    f.write(html)

    # read the debug file back
    #with open("manganelo.html", "r") as f:
    #    #html = f.read()

    # get title in the following form
    # <div class="story-info-right">
    # <h1>Murabito Desu Ga Nani Ka?</h1>

    try:
        # manganelo
        manga_title = re.search('(?:<div class="story-info-right">[\t\r\n ]*<h1>)(.*?)(?:[\t ]*<\\/h1>)', html)[1]
        is_manganelo = True
    except:
        # mangakakalot.gg
        manga_title = re.search('(?:<ul class="manga-info-text">[\t\r\n ]*<li>[\t\r\n ]*<h1>)(.*?)(?:[\t ]*<\\/h1>)', html)[1]
        is_manganelo = False

    # generate channel information
    locale.setlocale(locale.LC_TIME, 'en_US.UTF-8')
    channel_pub_date = datetime.now().strftime('%a, %d %b %Y %H:%M:%S +0000')
    channel = {
            "title": "NEL " + manga_title,
            "link": url,
            "description": "MangaNelo " + manga_title,
            "language": "en-us",
            "copyright": "MangaNelo",
            "pubDate": channel_pub_date,
            "image": {
                    "url": "https://manganelo.com/favicon.png",
                    "title": "MangaNelo",
                    "link": url,
                    },
            }

    if is_manganelo:
        # <li class="a-h">
        #   <a rel="nofollow" class="chapter-name text-nowrap" href="https://manganelo.com/chapter/murabito_desu_ga_nani_ka/chapter_38" title="Murabito desu ga Nani ka? chapter Chapter 38">Chapter 38</a>
        #   <span class="chapter-view text-nowrap">24,593</span>
        #   <span class="chapter-time text-nowrap" title="Dec 28,2020 04:12">Dec 28,20</span>
        # </li>
        re_list_elements = '(?:<li class="a-h">)(.*?)(?:</li>)'
        matches = re.findall(re_list_elements, html, re.DOTALL)
    else:
        re_list_elements = '(?:<div class="row">)(.*?)(?:</div>)'
        matches = re.findall(re_list_elements, html, re.DOTALL)

    if verbose:
        print("parse_site_manganelo: number of matches found in site: {}".format(len(matches)))
    if len(matches) == 0:
        raise ValueError("found no entries for site: '{}'".format(url))

    if is_manganelo:
        data = [parse_entry_manganelo(x, referrer, verbose) for x in matches]
    else:
        data = [parse_entry_mangakakalot(x, referrer, verbose) for x in matches]

    if verbose:
        print("parse_site_manganelo: generated data")
    return channel, data

def parse_site(url, referrer="", verbose=False):
    if verbose:
        print("parse_site: parsing url: '{}'".format(url))
    if url.startswith("https://www.mangaread.org"):
        parse_site_helper = parse_site_mangaread
    elif url.startswith("https://manganelo.com") \
        or url.startswith("https://manganato.com") \
        or url.startswith("https://readmanganato.com") \
        or url.startswith("https://chapmanganato.com") \
        or url.startswith("https://chapmanganato.to") \
        or url.startswith("https://www.natomanga.com") \
        :
        parse_site_helper = parse_site_manganelo
    elif url.startswith("https://imperfectcomic.com"):
        parse_site_helper = parse_site_imperfectcomic
    else:
        raise ValueError("parse_site: unsupported hoster-url: '{}'".format(url))
    return parse_site_helper(url, referrer, verbose)

def generate_rss(channel, data, output, verbose=False):
    template_path = os.path.join(os.path.dirname(os.path.abspath(__file__)), "rss.mako")
    with open(output, "wb") as f:
        try:
            reportTemplate = Template(filename=template_path, input_encoding='utf-8', output_encoding='utf-8')
            f.write(reportTemplate.render(
                    channel=channel,
                    data=data
            ))
        except Exception:
            print(mako.exceptions.text_error_template().render())
            sys.exit(1)

def main():
    parser = argparse.ArgumentParser()

    parser.add_argument("--url",
                        help="URL to Mangaread base.",
                        default="https://www.mangaread.org/manga/")
    parser.add_argument("--referrer",
                        help="referrer URL to prepend to the article URL.",
                        default="")
    parser.add_argument("-o", "--output",
                        help="RSS output file",
                        default="out.rss")
    parser.add_argument("--max-tries",
                        help="Maximum number of download/parse attempts",
                        type=int,
                        default=5)
    parser.add_argument("--max-entries",
                        help="Maximum number of RSS entries, 0 for no limit",
                        type=int,
                        default=20)
    parser.add_argument("-v", "--verbose", help="increase output verbosity",
                        action="store_true")
    args = parser.parse_args()

    if args.verbose:
        print("verbosity turned on")

    all_start = time.time()

    for i in range(args.max_tries):
        try:
            channel,data = parse_site(args.url, args.referrer, args.verbose)
            break
        except Exception as e:
            print("Exception in try {}: {}".format(i, e))
            if i == args.max_tries-1:
                raise
        except:
            print("Exception in try {}".format(i))
            if i == args.max_tries-1:
                raise
        time.sleep(5)
    if args.max_entries > 0:
        data_selected = data[0:args.max_entries]
    else:
        data_selected = data
    generate_rss(channel, data_selected, args.output, args.verbose)

    print("total runtime {:.3f} s".format(time.time() - all_start))

if __name__ == "__main__":
    main()

