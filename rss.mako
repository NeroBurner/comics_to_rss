<?xml version="1.0" encoding="utf-8"?>

<rss version="2.0">
  <channel>
  % for key in ["title", "link", "description", "copyright", "pubDate"]:
    <${key}>${channel[key]}</${key}>
  % endfor
    <image>
    % for key in ["url", "title", "link"]:
      <${key}>${channel["image"][key]}</${key}>
    % endfor
    </image>

  % for item in data:
    <item>
    % for key in ["title", "description", "link", "author", "pubDate"]:
      <${key}>${item[key]}</${key}>
    % endfor # key
    </item>
  % endfor # data

  </channel>
</rss>
