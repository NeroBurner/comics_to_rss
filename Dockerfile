# docker build -t registry.gitlab.com/neroburner/comics_to_rss .
FROM python:latest
RUN pip install --no-cache-dir requests mako \
  && apt-get update -qq \
  && apt-get install -y locales \
  && sed -i 's/^# *\(en_US.UTF-8\)/\1/' /etc/locale.gen \
  && sed -i 's/^# *\(de_AT.UTF-8\)/\1/' /etc/locale.gen \
  && locale-gen
